(function(trackerid,campaignid){
/**
 * HTTP endpoint callback
 * @const
 * @type {string}
 */
var trackerSettings_endpoint = 'http' + ((window.location.protocol == "https:") ? 's' : '') + '://localhost:9999';
/**
 * indicator that the browser has CORS support
 * @type {boolean}
 */
var trackerSettings_hasCorsSupport = true;
/**
 * timeout for the jsonp callbacks
 * @const
 * @type {number}
 */
var trackerSettings_jsonpFailTimeout = 5 * 1000;
/**
 * jsonp max packet size (254- (endpoint url size + '?q='))
 * @const
 * @type {number}
 */
var trackerSettings_jsonpMaxUrlLength = 200;
/**
 * FB check interval
 * @const
 * @type {number}
 */
var trackerSettings_fbCheckInterval = 3000;
/**
 * Fake object for a jsonp function
 * @type {function?}
 */
var makeJsonpRequest=null;
/*
 * Springtab cluster command hook
 * @type {function}
 */
//var tracker_cluster_hook=function(clusterid){tracker_cluster(clusterid);};
var tracker_cluster_hook=function(clusterid){tracker_cluster(clusterid);};
var tracker_cluster_hook_in=function(clusterid){tracker_cluster_in(clusterid);};
var trackerDbg = function(e) {};
_springtab=_springtab||[];
/**
 * Create the XHR object for send
 * @return {XMLHttpRequest|XDomainRequest}
 */
function createCORSRequestPOST() {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // XHR for Chrome/Firefox/Opera/Safari.
        xhr.open("POST", trackerSettings_endpoint + "/q" + campaignid + "?" + trackerid, true);
    } else if (typeof XDomainRequest != "undefined") {
        // XDomainRequest for IE.
        xhr = new XDomainRequest();
        xhr.open("POST", url);
    } else {
        // CORS not supported.
        xhr = null;
    }
    return xhr;
}

/**
 * Make the cors request
 * @param {string} data the POST's body
 * @param {function()} success_callback callback called on succes (the retuned data is the paramter)
 * @param {function()} error_callback callback called on CORS error
 */
function makeCorsRequest(data, success_callback, error_callback) {
    var xhr = createCORSRequestPOST();

    // Response handlers.
    xhr.onload = function() {
        var jsonData = JSON.parse(xhr.responseText);
        //direct commands (json parsed)
        for (var i = 0; i < jsonData.length; i++)
        {
            _springtab.push(jsonData[i]);
        }
        success_callback();
    };

    xhr.onerror = error_callback;
    xhr.ontimeout = error_callback;
    if (xhr.onabort)
    {
        xhr.onabort = error_callback;
    }
    xhr.send(data);
}

//test for CORS support
if (!createCORSRequestPOST())
    trackerSettings_hasCorsSupport = false;

/**
 * Generic call for send data
 * @param {string} data
 * @param {function} success_callback
 * @param {function} error_callback
 */

function sendData(data, success_callback, error_callback) {
    var jsonData = JSON.stringify(data);
    trackerDbg("sendData: " + jsonData);

    if (!success_callback)
        success_callback = function() {};

    if (!error_callback)
        error_callback = function() {
        };

    //switch method
    if (trackerSettings_hasCorsSupport)
    {
        makeCorsRequest(jsonData, success_callback, error_callback);
    } else
    if (makeJsonpRequest)
    {
        makeJsonpRequest(jsonData, success_callback, error_callback);
    } else {
        trackerDbg("Error: sendData failed");
    }
}
/**
 * Action for fb_me
 */
function tracker_fb_me()
{
    FB.api('/me', function(response) {
        sendData([['fbr_me', response]]);
    });
}
/**
 * Action for in_me
 */
function tracker_in_me()
{
    IN.API.Profile('me').result(function(result) {
        //console.log(result.value[0]);
        sendData([['inr_me', result.values[0]]]);
    });
}

/**
 * Action for fb_like
 * @param {number} plimit limit for the query
 * @param {string} pafter after parameter for fb
 * @returns {undefined}
 */
function tracker_fb_like(plimit, pafter)
{
    FB.api('/me/likes', {fields: 'id', limit: plimit, after: pafter}, function(response) {
        if (response.data)
        {
            //rearrange data
            var pureData = [];
            for (var i = 0; i < response.data.length; i++)
            {
                pureData.push(response.data[i].id);
            }
            
            var before= (response.paging) ? response.paging.cursors.before : null;
            var after= (response.paging) ? response.paging.cursors.after : null;
            
            //send both with the data
            sendData([['fbr_like', pafter, pureData, before , after]]);
        } else {
            //failed
            sendData([['fbr_like', pafter, null]]);
        }
    });
}

function tracker_in_position()
{   
    IN.API.Raw()
    .url('/people/~/positions')
    .method("GET")
    .result(function (result) {
        sendData([['inr_position', result.values[0]]]);
    })
    .error(function (error) {
        //console.log('Linkedin error', error);
    });
}

/**
 * Callback on FB login status 
 * @param {object} response from FB
 */
function fbLoginStatusCallback(response)
{
    if (response.status == "connected")
    {
        //signal the server, that we have Facebook
        FB.api('/me?fields=id,email,first_name,last_name', function(meresponse) {
            sendData([['fb_init', response.authResponse.userID,meresponse]]);
        });
    } else {
        //subscibe to login
        FB.Event.subscribe('auth.login', fbLoginStatusCallback);
    }
}

/**
 * Check if the FB object is done
 * @param tries how many tries passed
 */
function checkFbObject(tries)
{
    //if not defined, return and set timeout for the same function
    if (typeof FB == 'undefined')
    {
        trackerDbg("Checking FB: no FB object");

        //first 3 tries is fast
        if (tries < 3)
        {
            //50 ms
            setTimeout(function() {
                checkFbObject(tries + 1);
            }, 50);
        } else {
            //bigger timeout
            setTimeout(function() {
                checkFbObject(tries + 1);
            }, trackerSettings_fbCheckInterval);
        }
        return;
    }

    //have FB object
    trackerDbg("Checking FB: success");
    FB.getLoginStatus(fbLoginStatusCallback,true);
}
/**
 * Check if the FB object is done
 * @param tries how many tries passed
 */
function checkInObject(tries)
{
    //if not defined, return and set timeout for the same function
    console.log(IN);
    if (typeof IN.User === 'undefined')
    {
        console.log(tries);
        trackerDbg("Checking IN: no IN object");

        //first 3 tries is fast
        if (tries < 3)
        {
            //50 ms
            setTimeout(function() {
                checkInObject(tries + 1);
            }, 50);
        } else {
            //bigger timeout
            setTimeout(function() {
                checkInObject(tries + 1);
            }, trackerSettings_fbCheckInterval);
        }
        return;
    }

    //have FB object
    trackerDbg("Checking IN: success");
    if(IN.User.isAuthorized() ) {
        console.log('voa');
        IN.API.Profile('me').result(function(result) {
            sendData([['in_init', result.values[0]]]);
            console.log(result);
        });
    }
}
/**
 * Set extra email parameter in datastore
 * @param {string} email email address for the current user
 */
function setExtraEmail(email)
{
    sendData([['extra', 'email',email]]);
}

/**
 * Hiding dom elements with the specific cluster id
 * @param {type} clusterid
 */
function clusterDomHide(clusterid)
{
    var trDoms = document.getElementsByClassName("springtab");
    for (var i = 0; i < trDoms.length; i++)
    {
        var extendedName = " " + trDoms[i].className + " ";

        //check if we have the name in the list
        if (extendedName.indexOf("spt_" + clusterid) == -1)
        {
            trDoms[i].parentNode.removeChild(trDoms[i]);
            //restart loop
            i = 0;
        }
    }
}

//set the defaul cluster id callback
var clustercallback = clusterDomHide;


/**
 * Set the callback function
 * @param {function} cbfunction
 */
function tracker_callback(cbfunction)
{
    clustercallback = cbfunction;
}
/**
 * Handler for tr_cluster command
 * @param {number} clusterid the actual cluster
 */
function tracker_cluster(clusterid) {

    var completed = function() {
        clustercallback(clusterid);
    };

    //from jquery: ready.js:
    setTimeout(completed);

    if (document.readyState !== "complete") {
        // Use the handy event callback
        document.addEventListener("DOMContentLoaded", completed, false);

        // A fallback to window.onload, that will always work
        window.addEventListener("load", completed, false);
    }

    //wait until we have FB object
    checkFbObject(1);
}
/**
 * Handler for tr_cluster command
 * @param {number} clusterid the actual cluster
 */
function tracker_cluster_in(clusterid) {

    var completed = function() {
        clustercallback(clusterid);
    };

    //from jquery: ready.js:
    setTimeout(completed);

    if (document.readyState !== "complete") {
        // Use the handy event callback
        document.addEventListener("DOMContentLoaded", completed, false);

        // A fallback to window.onload, that will always work
        window.addEventListener("load", completed, false);
    }

    //wait until we have FB object
    checkInObject(1);
}
/**
 * The main command parser
 * @param {array} commands
 */
function parseTrackerCommands(commands) {
    for (var i in commands) {
        var oneCommand=commands[i];
        trackerDbg("Processing: " + oneCommand);
        //the great switch
        if (oneCommand[0] == 'trackerid_update')
        {
            trackerid = oneCommand[1];
            checkFbObject(1);
            continue;
        }

        if (oneCommand[0] == 'fb_me')
        {
            tracker_fb_me();
            continue;
        }

        if (oneCommand[0] == 'fb_like')
        {
            tracker_fb_like(oneCommand[1], oneCommand[2]);
            continue;
        }

        if (oneCommand[0] == 'in_me')
        {
            tracker_in_me();
            continue;
        }

        if (oneCommand[0] == 'in_position')
        {
            tracker_in_position();
            continue;
        }

        if (oneCommand[0] == 'tr_cluster')
        {
            tracker_cluster_hook(oneCommand[1]);
            continue;
        }

        if (oneCommand[0] == 'tr_cluster_in')
        {
            tracker_cluster_hook_in(oneCommand[1]);
            continue;
        }

        if (oneCommand[0] == 'spt_callback')
        {
            tracker_callback(oneCommand[1]);
            continue;
        }

        if (oneCommand[0] == 'extra_email')
        {
            setExtraEmail(oneCommand[1]);
            continue;
        }
        
        //hack for Peter
        if (oneCommand[0] == 'peterhack_likes')
        {
            if (typeof peterhack_likes != 'undefined')
            {
                peterhack_likes(oneCommand);
            }
            continue;
        }
        //hack is over

        //api 1.1
        if (oneCommand[0] == 'get_id'){
            var getid_cb=oneCommand[1];
            setTimeout(function(){ getid_cb(trackerid) },0);
            continue;
        }
    }
}

//process already sent commands
parseTrackerCommands(_springtab);

//override the dummy global object with some functions
_springtab = {
    push: function(command)
    {
        parseTrackerCommands([command]);
    }
};
})('FAKE-TRACKER-ID');