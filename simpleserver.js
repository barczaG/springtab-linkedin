var http = require('http');
var url = require('url');
var fs = require('fs');

function doHeaders(response) {
  var headers = {};
  headers["Access-Control-Allow-Origin"] = "*";
  headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
  headers["Access-Control-Allow-Credentials"] = 'true';
  headers["Access-Control-Max-Age"] = '86400'; // 24 hours
  headers["Access-Control-Allow-Headers"] = "X-Requested-With, Access-Control-Allow-Origin, X-HTTP-Method-Override, Content-Type, Authorization, Accept";
  headers["P3P"] = 'CP="HONK IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"';
  headers["Content-Type"] = "text/javascript";
  response.writeHead(200, headers);
}

http.createServer(function (request, response) {
  var parsed_url = url.parse(request.url);

  if (request.url.substring(0, 2) == "/t") {

    doHeaders(response);
    response.end(
      (fs.readFileSync('dist/modernbrowser.js') + "")
        .replace("'FAKE-TRACKER-ID');", "")
      + '"trackerid","1");_springtab.push(["tr_cluster",1]);');
  }

  if (parsed_url.pathname.substring(0, 2) == "/q") {


    if (request.method == 'POST') {
      var postbody = '';
      request.on('data', function (data) {
        postbody += data;
      });
      request.on('end', function () {
        doHeaders(response);
        var jsonData = JSON.parse(postbody);
        console.log(jsonData);

        if (postbody.indexOf('fb_init') > 0) {
          return response.end('[["fb_me"]]');
        }
        if (postbody.indexOf('fbr_me') > 0) {
          return response.end('[["fb_like",null,null]]');
        }

        if (postbody.indexOf('fbr_like') > 0) {
          return response.end('[]');
        }
      });
    }
  }

}).listen(process.env.PORT || 9999);

console.log("Server on :" + (process.env.PORT || 9999));