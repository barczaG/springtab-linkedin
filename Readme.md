# Springtab linkedin showcase

## How to use
- `git clone https://bitbucket.org/barczaG/springtab-linkedin.git`
- `cd springtab-linkedin`
- `npm i`
- `node server.js`
- Visit `http://localhost:9999/linkedin.html`