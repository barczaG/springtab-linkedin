'use strict';
const app = require('koa')();
const router = require('koa-router')();
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const parse = require('co-body');
const serve = require('koa-static');
const logger = require('koa-logger');
const cors = require('koa-cors');

//config
const port = process.env.PORT || 9999;

router.get('/t1.js', function *() {
	var file = yield fs.readFileAsync('dist/modernbrowser.js', 'utf-8');
	file = file.replace('\'FAKE-TRACKER-ID\');', '');

	if(this.query.linkedin)
		file += '"trackerid","1");_springtab.push(["tr_cluster_in",1]);';
	else 
		file += '"trackerid","1");_springtab.push(["tr_cluster",1]);';
	
	this.body = file;
});

router.post('/q1', function *() {

	var body = (yield parse.json(this))[0];
	var response;

	console.log(body);

	if (body[0] === 'fb_init') {
        response = [['fb_me']];
    }

    if (body[0] === 'fbr_me') {
        response = [['fb_like',null,null]];
    }

    if (body[0] === 'fbr_like') {
        response = [];
    }

    if (body[0] === 'in_init') {
        response = [['in_me']];
    }

    if (body[0] === 'inr_me') {
        response = [['in_position']];
    }

    if (body[0] === 'inr_position') {
        response = [];
    }
    this.body = response;
});

app.use(logger());
app.use(cors());
app
.use(router.routes())
.use(router.allowedMethods());

app.use(serve(__dirname + '/dist'));

if (!module.parent) {
	app.listen(port);
	console.log(`listening on port ${port}`);
}
